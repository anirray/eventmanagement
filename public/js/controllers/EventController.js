'use strict';

app.controller('EventController', function($scope, EventFactory){
	$scope.displayForm = true;
	$scope.searchEvents = '';

	//$scope.pastFilter = show events in the past via clicking button
	//$scope.futureFilter = show events in futureFilter
	//$scope.tbd = show events TBD

	var setEvents = function(events){
		$scope.events = events;
		console.log('events on scope', $scope.events)
	};

	$scope.rsvp = function(event){
		var firstName = event.guest.firstName;
		var lastName = event.guest.lastName;
		var eventID = event.id;
		console.log('called');
		console.log('guest', guest);
		// add guest to event in DB
		// create guest
	};



	$scope.createEvent = function(newEvent){
		console.log('newEvent', newEvent);
		EventFactory.postEvent(newEvent)
			.then(function(){
				return EventFactory.getEvents();
			})
			.then(function(events){
				setEvents(events.data);
			});
	};

	$scope.deleteEvent = function(event){
		console.log('event tb deleted', event);
		EventFactory.deleteEvent(event)
			.then(function(val){
				console.log('deletion', val);
				EventFactory.getEvents()
				.then(function(events){
					setEvents(events.data);
				});
			});
	};
	EventFactory.getEvents()
	.then(function(events){
		setEvents(events.data);
	});
});
