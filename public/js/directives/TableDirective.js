app.directive('tabledirective', function() {

    return {
        restrict: 'E',
        templateUrl: 'js/directives/templates/table.html'
    };

});
