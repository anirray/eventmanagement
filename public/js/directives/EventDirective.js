app.directive('eventdirective', function () {

    return {
        restrict: 'E',
        templateUrl: 'js/directives/templates/eventinput.html'
    };

});
