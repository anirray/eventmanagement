'use strict';
var app = angular.module('SplashApp', ['ui.router', 'ngTable'])
	.config(function($stateProvider){

	$stateProvider
		.state('event', {
	    url: '/eventinput',
	    templateUrl: '/templates/event.html',
	    controller: 'EventController'
    })
		.state('guests', {
			url:'guests',
			templateUrl: '/templates/guests.html',
			controller: 'GuestController'
		})
		.state('eventList', {
			url: 'eventList',
			templateUrl: '/templates/eventList.html',
			controller: 'EventController'
		});
	});
