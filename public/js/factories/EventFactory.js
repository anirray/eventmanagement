'use strict';

app.factory('EventFactory', function ($http) {

    return {
        getEvents: function () {
            return $http.get('/api/event');
        },


        postEvent: function(event){
            return $http.post('/api/event', event);
        },


        deleteEvent: function(event){
            return $http.delete('/api/event/' + event.id);
        },


        editEvent: function(event){
            return $http.put('/api/event/' + event.id, event);
        }
    };
});
