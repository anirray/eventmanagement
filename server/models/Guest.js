'use strict';

module.exports = function(sequelize, DataTypes) {
  var Guest = sequelize.define('Guest', {
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    firstName: DataTypes.DATE,
    lastName: DataTypes.DATE,

  });

  return Guest;
};
