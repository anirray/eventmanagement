'use strict';

module.exports = function(sequelize, DataTypes) {
  var Event = sequelize.define('Event', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    startTime:{
      type: DataTypes.STRING, // can keep date in stringified ISO
      defaultValue: 'TBD'
     },
    endTime: {
      type: DataTypes.STRING,
      defaultValue: 'TBD'
    }
  });

  return Event;
};
