'use strict';
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var app = express();
var models = require('./models');
var publicPath = path.join(__dirname, '../public');
var bowerPath = path.join(__dirname, '../bower_components');
var indexHtmlPath = path.join(__dirname, '../index.html');
var eventRouter = require('./routes/eventRoutes');


app.set('port', process.env.PORT || 3000);
app.use(express.static(publicPath));
app.use(express.static(bowerPath));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Error');
});


app.use(logger('dev'));
app.use('/api/event/', eventRouter);

app.get('/*', function(req, res) {
    res.sendFile(indexHtmlPath);
});

models.sequelize.sync({force: true})
  .then(function(){
    app.listen(app.get('port'), function(){
      console.log('Server listening on port ', app.get('port'));
    });
  });

module.exports = app;
