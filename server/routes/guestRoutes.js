'use strict';
var path = require('path');
var express = require('express');
var router = express.Router();
var models = require('../models');

router.get('/', function(req,res){
  models.Guest.findAll()
    .then(function(results){
      res.send(results);
    });

});
router.post('/', function(req,res){
  var guest = req.body;
  console.log('guest received', guest);
  models.Guest.create(guest)
    .then(function(createdGuest){
      res.send(createdGuest);
    });
  });

router.put('/:id', function(req,res){
  var updatedGuest = req.body;
  models.Guest.update(updatedGuest, {
    where: {
      id: req.params.id
    }
  })
  .then(function(rowsUpdated){
    res.send(rowsUpdated.toString());
  });
});

router.delete('/:id', function(req,res){
  models.Guest.destroy({where: {id: req.params.id}})
    .then(function(rowsDeleted){
      res.send(rowsDeleted.toString());
    });
});


module.exports = router;
