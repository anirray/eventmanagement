'use strict';
var path = require('path');
var express = require('express');
var router = express.Router();
var models = require('../models');

router.get('/', function(req, res) {
  models.Event.findAll()
    .then(function(results){
      res.send(results);
    });
});

router.post('/', function(req, res){
  var event = req.body;
  console.log('event received', event);
  models.Event.create(event)
    .then(function(createdEvent){
      res.send(createdEvent);
    });
});


router.put('/:id', function(req, res) {
    var updatedEvent = req.body;
    models.Event.update(updatedEvent, {
      where: {
        id: req.params.id
      }
    })
    .then(function(rowsUpdated){
      res.send(rowsUpdated.toString()); //sequelize does not return updated obj
    });

});

router.delete('/:id', function(req, res) {
  console.log('id to be deleted', req.params.id);
  models.Event.destroy({where: {id: req.params.id}})
    .then(function(rowsDeleted){
      console.log('deleted', rowsDeleted);
      res.send(rowsDeleted.toString());
    });
});

module.exports = router;
